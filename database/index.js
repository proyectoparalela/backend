var Pool = require('pg');

var pool = new Pool();

module.exports = {
    query: (text, params, callback) => {
        return pool.query(text, params, callback)
    }
};
