var express = require('express');
var bodyParser = require('body-parser');

/**
 * Inicializar express
 */
var app = express();

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

/**
 * Importar rutas
 */
var appRoutes = require('./routes/app');


/**
 * CORS
 */
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');

    next();
});

/**
 * Cargar rutas
 */
app.use('/api', appRoutes);



/**
 * Server Listener
 */
app.listen(3000, () => {
   console.log('Express: on');
});



